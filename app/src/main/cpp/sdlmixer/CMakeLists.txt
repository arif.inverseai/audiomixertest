# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.10.2)

# Declares and names the project.

project("sdlmixer")

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.
set(header ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/../sdl2/include)

FILE(GLOB src *.c)
#FILE(GLOB header *.h)
add_library(sdlmixer SHARED ${src})
target_include_directories(sdlmixer PUBLIC ${header})
target_link_libraries(sdlmixer ${log-lib} ${sdl2})